<?php 
	require_once('functions.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
		<meta charset="utf-8">
		<title>Deliverable 1 - php based gallery</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" href="style.css">
	</head>
	<body>

		<div class="container">
			<div id="img-row" class="row">
				<div class="gallery">
				<?php
					$i = 0;	
					$html = '';
					$returnedData = getData( jsonURL );
					//echo '<pre>';print_r($returnedData);echo '</pre>';

					foreach ($returnedData as $key => $value) {
						$html .= '<div class="item-box col-xs-6 cols-sm-5 col-md-3 box-'.$i++.'">
							<div class="box_gallery">
								<a target="_blank" href="http://www.pinkvilla.com'.$value['path'].'" class="info">
									<img class="img-responsive" 
										src="'.$value['image']['url'].'" 
										width="'.$value['image']['width'].'" 
										height="'.$value['image']['height'].'" 
									/>
								</a>
								<h4 class="title">'.$value['title'].'</h4>
								<span class="viewcount">Total Likes: '.$value['viewCount'].'</span>
							</div>
						</div>';

						if($i === LIMIT){
							break;
						}
					}
					echo $html;
				?>
				</div>
				<div class="col-sm-12 loader">
					<img style="display:none;" class="spinner" src="loader.gif" alt="loader"/>
				</div>
				<input type="hidden" id="first" value="<?php echo FIRST; ?>" />
				<input type="hidden" id="limit" value="<?php echo LIMIT; ?>" >

			</div>
		</div>

		<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
		<script type='text/javascript' src="https://cdnjs.cloudflare.com/ajax/libs/jquery.imagesloaded/4.1.4/imagesloaded.min.js"></script>
		<script type='text/javascript' src="script.js"></script>

	</body>
</html>

