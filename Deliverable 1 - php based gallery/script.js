( function($){ 
	$(document).ready(function(){ "use strict"; 
	
		$('.gallery').each(function(){
			let container = $(this);

			container.isotope({
				itemSelector: '.item-box',
				layoutMode: 'masonry',	//for pinterest packed layout
				// layoutMode: 'fitRows', 	// for post liked sorted order  
				resizesContainer: true
			});

		});

		$(window).resize(function(){
			$('.gallery').isotope();
		});

		$(window).on('scroll.infinite', loadMore);

	});

	

	function loopGallery(test, index, item){
		if(test){
			let box = $(`<div class="item-box col-xs-6 cols-sm-4 col-md-3 box-${index}"></div>`);
			let box_gallery = $(`<div class="box_gallery"></div>`);
			let aTag = $(`<a target="_blank" href="http://www.pinkvilla.com${item.path}" class="info"></a>`);
			let imgTag = $(`<img class="img-responsive" src="${item.wideImage.url}" width="${item.wideImage.width}" height="${item.wideImage.height}">`);
			let titleNlikes = $(`<h4 class="title">${item.title}</h4><span class="viewcount">Total Likes: ${item.viewCount}</span>`);

			box.append(box_gallery);
			box_gallery.append(aTag);
			aTag.append(imgTag);
			box_gallery.append(titleNlikes);

			$('.gallery').imagesLoaded( function(){
				$('.gallery').isotope('insert', $(box) );
			});
		}
	}

	function loadMore() {	
		if( $(window).scrollTop() == ($(document).height() - $(window).height()) ) {
			$(window).off('scroll.infinite');	

			let galleryLength = $('.box_gallery').length;
			let first = $('#first').val();
			let limit = $('#limit').val();

			let parameter = {
				start : galleryLength,
				limit : limit
			}

			$.ajax({
				url: "functions.php",
				type: "POST",
				data: parameter,
				dataType: 'json',
				success: function(data){
					//console.log(data);
					first = parseInt($('#first').val());
					limit = parseInt($('#limit').val());
					$('#first').val( first+limit );


					$.each(data, function(index, item){	
						loopGallery(index >= galleryLength && index < galleryLength + 20, index, item);
					});
					
					$(window).on('scroll.infinite', loadMore);
				},
				beforeSend: function(){
					$('.spinner').fadeIn();
				},
				complete: function(){
					$('.spinner').hide();
				}
			});
		}
	}

})(jQuery);