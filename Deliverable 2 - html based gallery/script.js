( function($){ "use strict";

	let corsAnyWhere = "https://cors-anywhere.herokuapp.com/";
	let encodeurl = corsAnyWhere+"https://cdn.pinkvilla.com/feed/fashion-section.json";

	$.getJSON(encodeurl, function(data){
		console.log(data);
		data = $(data).sort(sortJson);
		//console.log(data);		
		$.each(data, function(index, item){
			loopGallery(index <= 20, index, item);
		});
	});

	$(window).on('scroll.infinite', loadMore);

	$(window).resize(function(){
		$('.gallery').isotope();
	});

	function sortJson(item1, item2){
		return item1.viewCount > item2.viewCount ? 1 : -1;
	};

	function loopGallery(test, index, item){
		if(test){
			let box = $(`<div class="item-box col-xs-6 cols-sm-4 col-md-3 box-${index}"></div>`);
			let box_gallery = $(`<div class="box_gallery"></div>`);
			let aTag = $(`<a target="_blank" href="http://www.pinkvilla.com${item.path}" class="info"></a>`);
			let imgTag = $(`<img class="img-responsive" src="${item.image.url}" width="${item.image.width}" height="${item.image.height}">`);
			let titleNlikes = $(`<h4 class="title">${item.title}</h4><span class="viewcount">Total Likes: ${item.viewCount}</span>`);

			box.append(box_gallery);
			box_gallery.append(aTag);
			aTag.append(imgTag);
			box_gallery.append(titleNlikes);

			let $Grid = $('.gallery').imagesLoaded( function(){
				$Grid.isotope({
					itemSelector: '.item-box',
					layoutMode: 'masonry',	//for pinterest packed layout
					// layoutMode: 'fitRows', 	// for post liked sorted order  
					resizesContainer: true
				});

				$('.gallery').isotope('insert', $(box) );
			});
		}
	}

	function loadMore() {	
		if( $(window).scrollTop() == ($(document).height() - $(window).height()) ) {
			$(window).off('scroll.infinite');	
			let galleryLength = $('.box_gallery').length;
			$.ajax({
				url: encodeurl,
				type: "GET",
				dataType: 'json',
				success: function(data){
					//console.log(data);
					data = $(data).sort(sortJson);
					//console.log(data);
					$.each(data, function(index, item){	
						loopGallery(index >= galleryLength && index < galleryLength + 20, index, item);
					});
					
					$(window).on('scroll.infinite', loadMore);
				},
				beforeSend: function(){
					$('.spinner').fadeIn();
				},
				complete: function(){
					$('.spinner').hide();
				}
			});
		}
	}

})(jQuery);